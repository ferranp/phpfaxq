<?php
// $Id: index.php 2 2006-10-03 22:30:11Z ferran $
include('/usr/share/php/phtml/html.inc');
include('/usr/share/php/phtml/table.inc');

$SENDDIR='/var/spool/hylafax/sendq/';
$DONEDIR='/var/spool/hylafax/doneq/';
$DOCDIR='/var/spool/hylafax/docq/';

function fax_info($file)
{
        $fax = Array();
        $handle = fopen ($file, "r");

        if ($handle) {
          while (!feof ($handle)) {
            $buffer = fgets($handle, 4096);
            $a=split(":",$buffer,2);
            $fax[$a[0]]=$a[1];
          }
        fclose ($handle);
        return $fax;
        }
}

function fax_image($file,$tipo){
        
        global $q;
        $i=0;
        $fax = trim($file,"\n ");
        if (!is_file($fax)) return;

        $imatge = $q.$tipo.".%d.jpg";
        exec("rm *.jpg ; convert $fax $imatge");
        if ($handle = opendir(".")) {
            while (false !== ($file = readdir($handle))) { 
                if (preg_match("/jpg/",$file)){ 
                   $i++;          
                   $html=$html."<b>Pagina $i<b><br>";
                   $html=$html.html_image("Imatge del fax $q",$file);
                   $html=$html."<br>";
                } 
            }
            closedir($handle); 
        }
        return $html;
}


function mostrar_fax($file)
{
        $fax = fax_info($file);
        print table_row_array(Array($fax['jobid'],$fax['owner'],$fax['number'],
          $fax['totpages'],date("D d/m/Y H:i:s.",$fax['tts']),$fax['status'],
          "<small><a href=".$PHP_SELF."?action=view&q=".$fax['jobid'].">Veure</a>&nbsp;".
          "<a href=".$PHP_SELF."?action=del&q=".$fax['jobid'].">Esborrar</a></small>"));
}


if ((isset($action)) && ($action=="view") && (isset($q))) {
        
        print html_header('Veure fax '.$q,'/comu/style.css');
        print '<div align=center style="color: rgb(0, 0, 0); background-color: rgb(204, 204, 204);">';
        if (is_file($SENDDIR."q".$q)) $file=$SENDDIR."q".$q;
        elseif (is_file($DONEDIR."q".$q)) $file=$DONEDIR."q".$q;
        
        if (!isset($file))
        {
            print html_h1("Numero de fax incorrecte : $q");
            print html_footer();
            exit;
        }
        $fax = fax_info($file);
        
        print html_h1("Veure fax numero $q");
        
        if ($html=fax_image($DOCDIR.basename($fax['postscript']),"postscript")){
                print $html;
        }elseif($html=fax_image($DOCDIR.basename($fax['!postscript']),"!postscript")){
                print $html;
        }elseif($html=fax_image($DOCDIR.basename($fax['fax']),"fax")){
                print $html;
        }elseif($html=fax_image($DOCDIR.basename($fax['!fax']),"!fax")){
                print $html;
        }elseif($html=fax_image($DOCDIR.basename($fax['pdf']),"pdf")){
                print $html;
        }elseif($html=fax_image($DOCDIR.basename($fax['!pdf']),"!pdf")){
                print $html;
        }else print html_h1("Error al crear imatge");
        print '</div>';
        print html_footer();
        exit;
}
if ((isset($action)) && ($action=="del") && (isset($q))) {
        
        print html_header('Esborrar fax '.$q,'/comu/style.css');
        print html_h1("Esborrar fax numero $q");
        print "<pre>";
        system("echo apache | faxrm -a $q 2>&1 ");
        print "</pre>";
        print html_footer();
        exit;
}


print html_header('Gestio de faxos','/comu/style.css');
print html_h1('Gestio de Faxos');
print html_h2('Faxos Pendents d\'enviar');
print table_header('border=1 width=100%');
print "<tr><th width=5%>Job".
          "<th width=10%>Usuari".
          "<th width=10%>telefon".
          "<th width=5%>Pagines".
          "<th width=20%>Data/Hora".
          "<th>status".
          "<th width=8%>accions</tr>";

if ($handle = opendir($SENDDIR)) {

    while (false !== ($file = readdir($handle))) { 
        if ($file != "." && $file != ".." && $file != "seqf") { 
            mostrar_fax($SENDDIR.$file); 
        } 
    }
    closedir($handle); 
}
print table_footer();
    

print html_h2('Faxos Enviats o erronis');
print table_header('border=1 width=100%');
print "<tr><th width=5%>Job".
          "<th width=10%>Usuari".
          "<th width=10%>telefon".
          "<th width=5%>Pagines".
          "<th width=20%>Data/Hora".
          "<th>status".
          "<th width=8%>accions</tr>";

if ($handle = opendir($DONEDIR)) {

    while (false !== ($file = readdir($handle))) { 
        if ($file != "." && $file != ".." && $file != "seqf") { 
            mostrar_fax($DONEDIR.$file); 
        } 
    }
    closedir($handle); 
}
print table_footer();

print html_footer();

?>
